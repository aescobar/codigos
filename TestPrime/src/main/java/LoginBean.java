import org.primefaces.model.UploadedFile;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.util.ArrayList;

/**
 * Created by ariel on 5/23/16.
 */
@ViewScoped
@ManagedBean(name = "loginBean")
public class LoginBean {

    private ArrayList<Prueba> lstPruebas;

    public ArrayList<Prueba> getLstPruebas() {
        return lstPruebas;
    }

    public void setLstPruebas(ArrayList<Prueba> lstPruebas) {
        this.lstPruebas = lstPruebas;
    }

    @PostConstruct
    private void init(){
        lstPruebas = new ArrayList<Prueba>();
        lstPruebas.add(new Prueba("d1","d2.....",2, true));
        lstPruebas.add(new Prueba("d2","dxxx.....",3, false));
        lstPruebas.add(new Prueba("d3","dxxx.....",4, false));
        lstPruebas.add(new Prueba("d4","dxxx.....",5, false));

    }

    private UploadedFile file;

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public void upload() {
        if(file != null) {
            FacesMessage message = new FacesMessage("Succesful", file.getFileName() + " is uploaded.");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
    }
}
