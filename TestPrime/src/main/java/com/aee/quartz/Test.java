package com.aee.quartz;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

/**
 * Created by ariel on 5/30/16.
 */
public class Test {
    public static void main(String []args){
        JobKey jobKeyA = new JobKey("jobA", "group1");
        JobDetail jobA = JobBuilder.newJob(TestHoraJob.class)
                .withIdentity(jobKeyA).build();



        Trigger trigger1 = TriggerBuilder
                .newTrigger()
                .withIdentity("dummyTriggerName1", "group1")
                .withSchedule(
                        CronScheduleBuilder.cronSchedule("0 14 1 * * ?"))
                .build();

        Scheduler scheduler = null;
        try {
            scheduler = new StdSchedulerFactory().getScheduler();
            scheduler.start();
            scheduler.scheduleJob(jobA, trigger1);
        } catch (SchedulerException e) {
            e.printStackTrace();
        }



    }
}
