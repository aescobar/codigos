/**
 * Created by ariel on 5/23/16.
 */
public class Prueba {

    private String d1;
    private String d2;
    private int d3;
    private boolean d4;

    public Prueba(String d1, String d2, int d3, boolean d4) {
        this.d1 = d1;
        this.d2 = d2;
        this.d3 = d3;
        this.d4 = d4;

    }

    public boolean isD4() {
        return d4;
    }

    public void setD4(boolean d4) {
        this.d4 = d4;
    }

    public String getD1() {
        return d1;
    }

    public void setD1(String d1) {
        this.d1 = d1;
    }

    public String getD2() {
        return d2;
    }

    public void setD2(String d2) {
        this.d2 = d2;
    }

    public int getD3() {
        return d3;
    }

    public void setD3(int d3) {
        this.d3 = d3;
    }
}
